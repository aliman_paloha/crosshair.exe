﻿### CrossHair Application

#### youtube video
[![CrossHair youtube video](https://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://youtu.be/zvMwBgP6wrA)

#### download link
https://bitbucket.org/aliman_paloha/crosshair.exe/raw/master/CrossHair/setup.exe

have fun:)

This work has cost a lot of coffee. If you like my work, you are welcome to donate coffee.

Diese Arbeit hat viel Kaffee gekostet. Wenn Dir meine Arbeit gefällt, kannst Du mir gerne einen Kaffee spenden.

Questo lavoro è costato molto caffè. Se vi piace il mio lavoro, siete i benvenuti a donare il caffè.

Este trabajo ha costado mucho café. Si te gusta mi trabajo, eres bienvenido a donar café.

Эта работа стоила много кофе. Если вам нравятся мои работы, можете пожертвовать кофе.

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=UDGAYEHXADCS6&source=url)
![QR-Code](QR-Code.png)
